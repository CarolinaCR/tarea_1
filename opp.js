// ES5 -> ES6

// El uso de var a partir de ES6 esta prohibido
// var http = require ("http");
const http = require ("http");

// Primera implementacion:

function server (request, response){
  response.writeHead(200, {"Content-Type": "text/html"});
  // el 200 es codigo de estado, la familia de 200 es todo ocurrio correctamente
  response.write("Hola Mundo!!.");
  response.end();
}

// Para ejecutar el servidor:
http.createServer(server).listen(8080);
// Para entrar ir al navegador y poner http://localhost:8080/


// Segunda implementacion:

http.createServer(server).listen(8080);

http.createserver (function(request, response){
  response.writeHead(200, {"Content-Type": "text/html"});
  response.write("Hola Mundo!!.");
  response.end();
}).listen(8080);


// Tercera implementacion:

http.createServer(server).listen(8080);

http.createserver ((request, response) => {
  response.writeHead(200, {"Content-Type": "text/html"});
  response.write("Hola Mundo!!.");
  response.end();
}).listen(8080);
